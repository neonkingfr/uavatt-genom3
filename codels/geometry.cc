/*
 * Copyright (c) 2022 LAAS/CNRS
 * All rights reserved.
 *
 * Redistribution  and  use  in  source  and binary  forms,  with  or  without
 * modification, are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of  source  code must retain the  above copyright
 *      notice and this list of conditions.
 *   2. Redistributions in binary form must reproduce the above copyright
 *      notice and  this list of  conditions in the  documentation and/or
 *      other materials provided with the distribution.
 *
 * THE SOFTWARE  IS PROVIDED "AS IS"  AND THE AUTHOR  DISCLAIMS ALL WARRANTIES
 * WITH  REGARD   TO  THIS  SOFTWARE  INCLUDING  ALL   IMPLIED  WARRANTIES  OF
 * MERCHANTABILITY AND  FITNESS.  IN NO EVENT  SHALL THE AUTHOR  BE LIABLE FOR
 * ANY  SPECIAL, DIRECT,  INDIRECT, OR  CONSEQUENTIAL DAMAGES  OR  ANY DAMAGES
 * WHATSOEVER  RESULTING FROM  LOSS OF  USE, DATA  OR PROFITS,  WHETHER  IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR  OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 *                                           Anthony Mallet on Fri Jun 17 2022
 */
#include <cmath>

#include "Eigen/Dense"

#include "codels.h"


/* --- uavatt_gtmrp_allocmatrix -------------------------------------------- */

genom_event
uavatt_gtmrp_allocmatrix(int rotors,
                         double cx, double cy, double cz,
                         double armlen, double rx, double ry, double rz,
                         double cf, double ct,
                         double G[6 * or_rotorcraft_max_rotors],
                         const genom_context self)
{
  using namespace Eigen;

  Map< Matrix<double, 6, or_rotorcraft_max_rotors, RowMajor> > G_(G);
  Vector3d z, p;
  int i, sign;

  if (rotors < 0 || rotors > or_rotorcraft_max_rotors) {
    uavatt_e_inval_detail d = { "bad number of rotors" };
    return uavatt_e_inval(&d, self);
  }

  for(i = 0, sign = 1; i < rotors; i++, sign = -sign) {
    /* thrust direction */
    z =
      (AngleAxisd(2 * i * M_PI / rotors, Vector3d::UnitZ())
       * AngleAxisd(sign * rx, Vector3d::UnitX())
       * AngleAxisd(ry, Vector3d::UnitY())).matrix().col(2);

    /* thrust position */
    p =
      armlen *
      AngleAxisd(2 * i * M_PI / rotors, Vector3d::UnitZ()).matrix().col(0)
      + Vector3d(cx, cy, cz);

    /* wrench */
    G_.col(i) <<
      cf * z,
      cf * p.cross(z) - sign * rz * ct * z;
  }

  for(; i < or_rotorcraft_max_rotors; i++)
    G_.col(i) << Vector3d::Zero(), Vector3d::Zero();

  return genom_ok;
}


/* --- uavatt_invert_G ----------------------------------------------------- */

void
uavatt_invert_G(const double G[6 * or_rotorcraft_max_rotors],
                double iG[or_rotorcraft_max_rotors * 6])
{
  using namespace Eigen;

  Map< const Matrix<double, 6, or_rotorcraft_max_rotors, RowMajor> > G_(G);
  Map< Matrix<double, or_rotorcraft_max_rotors, 6, RowMajor> > iG_(iG);

  iG_ = G_.
    jacobiSvd(ComputeFullU | ComputeFullV).
    solve(Matrix<double, 6, 6>::Identity());
}


/* --- uavatt_Gw2 ---------------------------------------------------------- */

void
uavatt_Gw2(const double G[6 * or_rotorcraft_max_rotors], const double w,
           double f[6])
{
  using namespace Eigen;

  Map< const Matrix<double, 6, or_rotorcraft_max_rotors, RowMajor> > G_(G);
  Map< Matrix<double, 6, 1> > f_(f);

  f_ = G_ * Matrix<double, or_rotorcraft_max_rotors, 1>::Constant(w * w);
}
